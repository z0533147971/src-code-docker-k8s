import axios from "axios";
import {CheckQuantity} from "../types/types";
const ERP_BASE_URL =
  process.env.ERP_BASE_URL || "https://erp-server-v2.onrender.com";
import {products} from "./products";

export const getProductsFromDB = async () => {
  try {
    if (process.env.MODE !== "development") {
      const {data} = await axios.get(
        `${ERP_BASE_URL}/shop_inventory?searchText=`
      );
      return data;
    }
    return products;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getProductByIdFromDB = async (productId: number) => {
  try {
    if (process.env.MODE !== "development") {
      const {data} = await axios.get(
        `${ERP_BASE_URL}/shop_inventory/${productId}`
      );
      return data;
    }
    return products.find((product) => product.id === productId);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const checkStockInDB = async (cart: CheckQuantity[]) => {
  try {
    const {data} = await axios.post(
      `${ERP_BASE_URL}/shop_inventory/updateInventory`,
      cart
    );
    return data;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const cancelOrder = async (cart: CheckQuantity[]) => {
  try {
    const {data} = await axios.post(
      `${ERP_BASE_URL}/shop_inventory/cancelOrder`,
      cart
    );
    return data;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const connectedToERP = async () => {
  const url = `${ERP_BASE_URL}/api/connect`;
  let isConnect = false;
  let retries = 5;
  while (!isConnect && retries > 0) {
    try {
      const {data} = await axios.get(url);
      isConnect = true;
      console.log(data);
    } catch (error) {
      error instanceof Error &&
        console.error("Error connecting to oms:", error.message);
      console.log(`Retries left: ${--retries}`);
      await new Promise((resolve) => setTimeout(resolve, 5000)); // Wait for 5 seconds before retrying
    } finally {
      console.log(url);
    }
  }
  if (!isConnect) {
    console.error("Failed to connect to erp after multiple attempts.");
  }
};
