import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();

export const connectToDatabase = async (R_NUM = 5): Promise<void> => {
  const dbHost = process.env.MONGO_URI || "";
  let isConnect = false;
  let retries = R_NUM;
  while (!isConnect && retries > 0) {
    try {
      await mongoose.connect(dbHost);
      isConnect = true;
      console.log("Connected to MongoDB");
    } catch (error) {
      console.error("Error connecting to MongoDB:", error);
      console.log(`Retries left: ${--retries}`);
      await new Promise((resolve) => setTimeout(resolve, 5000)); // Wait for 5 seconds before retrying
    }
  }
  if (!isConnect) {
    console.error("Failed to connect to MongoDB after multiple attempts.");
  }
};
